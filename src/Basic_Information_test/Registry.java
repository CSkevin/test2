
package Basic_Information_test;

/*
 *
 * @author Kevin Fraser
 */

 import java.awt.event.ActionEvent;
 import java.awt.event.ActionListener;
 import javax.swing.*;
 import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;




 import javax.swing.border.TitledBorder;
 import java.util.logging.*;
import java.util.ArrayList;
public class Registry extends JFrame  implements Serializable 
{//Start of BankRecods Class
    

   
    //Text Fields
    private JTextField F=new JTextField(20);
    private  JTextField L=new JTextField(20);
    private  JTextField C=new JTextField(20);

    private  JTextField A=new JTextField(20);
    private  JTextArea displayArea=new JTextArea();
    //A numb name submit 
    private  JButton numb=new JButton( "Submit" );
    private  JButton load= new JButton ("load");
    private JScrollPane scroller = new JScrollPane();
    private  Dimension area; //indicates area taken up by graphics
    private Student register=new Student();
    ArrayList list =new ArrayList();
    
     String str="";
    
    public Registry()
    {// Start of  BasicInfor GUI Contructor
     
   
          
   
    //Panel One
    
    JPanel P1 = new JPanel();
      P1.add(displayArea);
      displayArea.setPreferredSize(new Dimension(500,200));//Becomes Scoller Size
      P1.setBorder(new TitledBorder("New Student"));
      displayArea.setEditable(false);//False value make Diplay Area not editable
       
       
      
        //Text Display Area, becomes the scroll pane's client
        scroller = new JScrollPane(displayArea);
        scroller.setPreferredSize(new Dimension(500,200));//Becomes Display Size
        
      
   
       

      
       
      
        
        
        
//Adds the scroll pane to Panel One
        P1.add(scroller , BorderLayout.NORTH);
      /*
        *Call revalidate on the Text Display Area to let  scroll pane  
        * the update  scroll bars.
        */
        
        
             
       
    
      //  Tip Information
      
      numb.setToolTipText("Click this button to display your information.");
     

    
     
   // Panel Two
    JPanel P2 =new JPanel();
     //Flow layout code set size for textfields
     
     FlowLayout flow1 = new FlowLayout();
     flow1.setAlignment(FlowLayout.LEFT);
        flow1.setHgap(10);//properties  can be changed dynamically
        flow1.setVgap(20);
      P2.setLayout(flow1);
    P2.add(new JLabel("Student ID"));
    P2.add(F);
    P2.add(new JLabel("Full Name"));
    P2.add(L);
    P2.add(new JLabel("telephone"));
    P2.add(C);
    
    P2.add(new JLabel("Address"));
    P2.add(A);
       
 
    
 
  
    //Panel Three 
    JPanel P3 =new JPanel(new FlowLayout(FlowLayout.RIGHT));
    P3.add(numb);
    P3.add(load);
  
    
    
    //Panels One,Two,and Three added to Frame
    add(P1,BorderLayout.NORTH);
    add(P2);
    add(P3,BorderLayout.SOUTH);
    
  numb.addActionListener(new ButtonListener());
  load.addActionListener(new LoadListener ());
    
    }//End of BasicInfor GUI Contructor
  
    
   
  
    private class ButtonListener implements ActionListener
    {// Start of Class ButtonListener implements ActionListener
    public void actionPerformed(ActionEvent e) 
    {// Start of action performed Method
  
      
        
        
   String a =F.getText();
   String b =L.getText();
   String c =C.getText(); 
  
   String w =A.getText();
        
      

// Start of First Conditions
       if (a.isEmpty() ) 
       
       {
          JOptionPane.showMessageDialog(null," First name entry short"); 
       
       F.setToolTipText("First Name Goes Here "
                + " e.g John as in John Smith"   );
       
       
       }
       
           
        
       
       else if (b.isEmpty() ) 
       
       {
          JOptionPane.showMessageDialog(null," Last name entry short"); 
       
       L.setToolTipText(" Last Name Goes Here"
                   +"e.g Smith as in John Smith");
       
       
       }
           
       else if (c.isEmpty() ) 
       
       {
          JOptionPane.showMessageDialog(null," Contact  entry short"); 
       C.setToolTipText("Must be a phone Number" 
                   +   " 868-555-1212");
       
       }
        
      
       
       
      else if (w.isEmpty() ) 
       
       {
          JOptionPane.showMessageDialog(null," Address entry short"); 
       
       A.setToolTipText(" Must be a Formal Address"
              + "E.g House Number or Building,Street Name,Locality,Trindad and Tobago ");
       /*  
              From Address Doctor
      https://www.addressdoctor.com/en/countries-data/address-formats.html#fbid=aTvN2tQQ4WX
*/
       
       
       }
        //End of First Conditions     
       
       
       
       else
       {//Start of Final Condition
      
       
            
          register =new Student(F.getText(), L.getText() ,C.getText(),A.getText() );
           
   
      
     //Dynamically Changing the Client's Size
        if (true) {
            
            
           //First Set the Client PreferredSize.
            // Area is zero by default
            //DisplayArea size is set by the Constructor
            displayArea.setPreferredSize(area);

            
            //Second Call revalidate on the Client
            //Let the scroll pane know to update itself
            //and its scrollbars.
            scroller.revalidate();
        }
          //Add graphics to Display Area  
        
        
        
     
/*
            Above scroll up date code  from
  https://docs.oracle.com/javase/tutorial/uiswing/components/scrollpane.html
*/




       
      
        //Clear the text components.
         F.setText("");
         L.setText("");
         C.setText("");
        
         A.setText("");
         
        //Return the focus to the typing area.
        F.requestFocusInWindow()  ;
       
       }//End of  Final Condition
       
     str=register.toString();
      
         
       //Information is appended to the TextArea via the objects ToString Method
    
    
     displayArea.append(str+"\n");
     
     displayArea.revalidate();
     
     
     
     
   
   }// End of action performed Method  
        
 }//End of Class ButtonListener implements ActionListener

    
    
      
    private class LoadListener implements ActionListener
    {// Start of Class ClearListener implements ActionListener
        @Override
        public void actionPerformed(ActionEvent e) 
        {// Start of action performed Method
              
              FileInputStream inFile=null;
               ObjectInputStream inStream=null;
            String str1="";
            try{ //try
           inFile= new FileInputStream ("./StudentsSaves.txt");
        inStream=new ObjectInputStream (inFile);
        list=(ArrayList)inStream.readObject();
        
       
        for (int i=0; i<list.size();i++)
           {
               System.out.println(list.get(i));
               str1=list.get(i).toString();
               
           }
        System.out.println("Row Count"+list.size());
            
        
             }//try
               catch(IOException o) //general catch clause specified
                {//catch block begin
                    System.out.println("Exception");
                } catch (ClassNotFoundException ex) {
           Logger.getLogger(Student.class.getName()).log(Level.SEVERE, null, ex);
            }//catch block end//catch block end
         
          displayArea.append(str1);
    
        
        
        }
    
    }
  
    
    
    
    

    
   
    
    
    
    
    
    
    
    
    
  public static void createAndShowGUI() 
  {//Start
        
   //Create and set up the window.
        Registry frame =new Registry();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("");
        frame.setSize(100,100);
        frame.setLocationRelativeTo(null);
        
       
       //Display the Window
        frame.pack();
        frame.setVisible(true);
      
        
 }//End    
  
  public  void TRYthis() 
  { //Start of Main     
      
     
   
/*
      The look and feel is set by calling the class method setLookAndFeel of the
UIManager class
                                          */
        
      try {
UIManager.setLookAndFeel(
"javax.swing.plaf.nimbus.NimbusLookAndFeel");
} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
Logger.getLogger(Registry.class.getName()).log(Level.SEVERE,
null, ex);
}
      
       //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() 
        { //Implement Method From java.lang.Runnable
            @Override
            public void run() 
            {//Start
                createAndShowGUI();
               
            }//End
        } 
          );
    }//End of  Main
  
            
      
}//End of Registry Class

 class Driver {
    
    public static void main(String[] args) {
        // TODO code application logic here
        Registry record=new Registry();
        record.TRYthis();
        
    
    
    }
}



