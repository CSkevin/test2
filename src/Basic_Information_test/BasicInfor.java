/*
Name: Kevin Fraser Student I.D:00026876 
Course: ITEC 236-Object Oriented Programming II
CRN:14700 Date:12/16/2017  Assignment: Write to and Read for a file using Serialization
Program Name: Basic Information


Purpose of Program:
Write an Arraylist Object to a file and than read a Serialized arraylist from a file.
The data from the file is than display in an area in the Program Screen.




 */
package Basic_Information_test;

/*
 *Source Code snippets 
 *From The Java SE Tutorials. http://docs.oracle.com/javase/8/
 * @author Kevin Fraser
 */
import Basic_Information_test.Person;
 import java.awt.event.ActionEvent;
 import java.awt.event.ActionListener;
 import javax.swing.*;
 import java.awt.*;
import java.io.FileInputStream;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
 import javax.swing.border.TitledBorder;
 import java.util.logging.*;
import java.util.ArrayList;
public class BasicInfor extends JFrame implements Serializable  
{//Start of BankRecods Class
    

   
    //Text Fields
    private JTextField F=new JTextField();
    private  JTextField L=new JTextField();
    private  JTextField C=new JTextField();
    private  JTextField S=new JTextField();
    private  JTextField A=new JTextField();
    private  JTextArea displayArea=new JTextArea();
    //A numb name submit 
    private  JButton numb=new JButton( "Submit" );
    private JScrollPane scroller = new JScrollPane();
    private  Dimension area; //indicates area taken up by graphics
    private Person Objects=new Person();
    static ArrayList list=new ArrayList();
    static FileOutputStream outFile=null;
    static ObjectOutputStream outStream=null;
    static FileInputStream inFile=null;
   static ObjectInputStream inStream=null;
     String str="";
    
    public BasicInfor()
    {// Start of  BasicInfor GUI Contructor
     
   
          
   
    //Panel One
    
    JPanel P1 = new JPanel();
      P1.add(displayArea);
      displayArea.setPreferredSize(new Dimension(500,1000));//Becomes Scoller Size
      P1.setBorder(new TitledBorder("Basic Information"));
      displayArea.setEditable(false);//False value make Diplay Area not editable
       
       
      
        //Text Display Area, becomes the scroll pane's client
        scroller = new JScrollPane(displayArea);
        scroller.setPreferredSize(new Dimension(500,200));//Becomes Display Size
        
      
   
       

      
       
      
        
        
        
//Adds the scroll pane to Panel One
        P1.add(scroller , BorderLayout.NORTH);
      /*
        *Call revalidate on the Text Display Area to let  scroll pane  
        * the update  scroll bars.
        */
        
        
             
       
    
      //  Tip Information
      
      numb.setToolTipText("Click this button to display your information.");
     

    
     
   // Panel Two
    JPanel P2 =new JPanel(new GridLayout(3,2));
    P2.add(new JLabel("First Name"));
    P2.add(F);
    P2.add(new JLabel("Last Name"));
    P2.add(L);
    P2.add(new JLabel("Contact"));
    P2.add(C);
    P2.add(new JLabel("Sex"));
    P2.add(S);
    P2.add(new JLabel("Address"));
    P2.add(A);
       
 
    
 
  
    //Panel Three 
    JPanel P3 =new JPanel(new FlowLayout(FlowLayout.RIGHT));
    P3.add(numb);
    
    
    //Panels One,Two,and Three added to Frame
    add(P1,BorderLayout.NORTH);
    add(P2);
    add(P3,BorderLayout.SOUTH);
    
  numb.addActionListener(new ButtonListener());
    
    }//End of BasicInfor GUI Contructor
  
    
   
  
    private class ButtonListener implements ActionListener
    {// Start of Class ButtonListener implements ActionListener
    public void actionPerformed(ActionEvent e) 
    {// Start of action performed Method
  
      
        
        
   String a =F.getText();
   String b =L.getText();
   String c =C.getText(); 
   String d =S.getText();
   String w =A.getText();
        
      /*Note - if and else if conditions assume that the User will Enter Personal
        Information in Sequence or in Conventional form.
   
        Tool-Tip Messages are activated when a Field is Empty. 
        Tool-Tip Messages are activated in sequence starting from First Name 
        Field.
   
      */

// Start of First Conditions
       if (a.isEmpty() ) 
       
       {
          JOptionPane.showMessageDialog(null," First name entry short"); 
       
       F.setToolTipText("First Name Goes Here "
                + " e.g John as in John Smith"   );
       
       
       }
       
           
        
       
       else if (b.isEmpty() ) 
       
       {
          JOptionPane.showMessageDialog(null," Last name entry short"); 
       
       L.setToolTipText(" Last Name Goes Here"
                   +"e.g Smith as in John Smith");
       
       
       }
           
       else if (c.isEmpty() ) 
       
       {
          JOptionPane.showMessageDialog(null," Contact  entry short"); 
       C.setToolTipText("Must be a phone Number" 
                   +   " 868-555-1212");
       
       }
        
      else if (d.isEmpty() ) 
       
       {
          JOptionPane.showMessageDialog(null," Sex entry short"); 
       
       S.setToolTipText(" This Field is required only"
                       +" Male or Female or Other");
       
       
       }
       
       
      else if (w.isEmpty() ) 
       
       {
          JOptionPane.showMessageDialog(null," Address entry short"); 
       
       A.setToolTipText(" Must be a Formal Address"
              + "E.g House Number or Building,Street Name,Locality,Trindad and Tobago ");
       /*  
              From Address Doctor
      https://www.addressdoctor.com/en/countries-data/address-formats.html#fbid=aTvN2tQQ4WX
*/
       
       
       }
        //End of First Conditions     
       
       
       
       else
       {//Start of Final Condition
      
       
            
          Objects =new Person(F.getText(), L.getText() ,C.getText(),S.getText() ,A.getText());
           
   
      
     //Dynamically Changing the Client's Size
        if (true) {
            
            
           //First Set the Client PreferredSize.
            // Area is zero by default
            //DisplayArea size is set by the Constructor
            displayArea.setPreferredSize(area);

            
            //Second Call revalidate on the Client
            //Let the scroll pane know to update itself
            //and its scrollbars.
            scroller.revalidate();
        }
          //Add graphics to Display Area  
        
        
        
     
/*
            Above scroll up date code  from
  https://docs.oracle.com/javase/tutorial/uiswing/components/scrollpane.html
*/




       
      
        //Clear the text components.
         F.setText("");
         L.setText("");
         C.setText("");
         S.setText("");
         A.setText("");
         
        //Return the focus to the typing area.
        F.requestFocusInWindow()  ;
       
       }//End of  Final Condition
       
       
       list.add(Objects.toString());
       try{
         //output to file
        outFile= new FileOutputStream("./Disk.txt");
        outStream=new ObjectOutputStream (outFile);
       
        outStream.writeObject(list); 
       
         outStream.close();
         
         
         //input from file
         
         inFile= new FileInputStream ("./Disk.txt");
        inStream=new ObjectInputStream (inFile);
        list=(ArrayList)inStream.readObject();
        
       
        for (int i=0; i<list.size();i++)
           {
               System.out.println(list.get(i));
               str=list.get(i).toString();
               
           }
        System.out.println("Row Count"+list.size());
        
       }
          
       catch(IOException o) //general catch clause specified
                {//catch block begin
                    System.out.println("Exception");
                } catch (ClassNotFoundException ex) {
           Logger.getLogger(BasicInfor.class.getName()).log(Level.SEVERE, null, ex);
       }//catch block end//catch block end
         
       //Information is appended to the TextArea via the objects ToString Method
    displayArea.append(str);
    
   
   }// End of action performed Method  
        
 }//End of Class ButtonListener implements ActionListener

    
    
  
  
  public static void createAndShowGUI() 
  {//Start
        
   //Create and set up the window.
        BasicInfor frame =new BasicInfor();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("");
        frame.setSize(100,100);
        frame.setLocationRelativeTo(null);
        
       
       //Display the Window
        frame.pack();
        frame.setVisible(true);
      
        
 }//End    
  
  public  void TRYthis() 
  { //Start of Main     
      
     
   
/*
      The look and feel is set by calling the class method setLookAndFeel of the
UIManager class
                                          */
        
      try {
UIManager.setLookAndFeel(
"javax.swing.plaf.nimbus.NimbusLookAndFeel");
} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
Logger.getLogger(BasicInfor.class.getName()).log(Level.SEVERE,
null, ex);
}
      
     
      
       //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() 
        { //Implement Method From java.lang.Runnable
            public void run() 
            {//Start
                createAndShowGUI();
               
            }//End
        } 
          );
    }//End of  Main
  
            
      
}//End of BasicInfor Class

